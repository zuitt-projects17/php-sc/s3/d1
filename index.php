<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S3: Classes and Objects</title>
</head>
<body>

	<h1>Classes from a Variable</h1>
	<p><?= $buildingObj->name; ?></p>

	<h1>Objects from Classes</h1>
	<p><?= $bldgOne->getDetails(); ?></p>

	<h1>Inheritance</h1>
	<p><?= $condOne->name; ?></p>
	<p><?= $condoOne->floors; ?></p>

	<h1>Polymorphism</h1>
	<p><?= $condoOne->getDetails(); ?></p>

</body>
</html>